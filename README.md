# README #

SimpleSyndicate.DependencyInjection.SimpleInjector NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.DependencyInjection.SimpleInjector

### What is this repository for? ###

* ?

### How do I get started? ###

* See the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.dependencyinjection.simpleinjector/issues?status=new&status=open
